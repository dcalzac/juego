import pygame
pygame.init()

ANCHO = 800
ALTO = 600

ventana = pygame.display.set_mode((500, 500))

# Colores
BLANCO = (255, 255, 255)
NEGRO = (0, 0, 0)
ROJO = (255, 0, 0)
VERDE = (0, 255, 0)
AZUL = (0, 0, 255)


pos_x = 100
pos_y = 100




jugar = True

while jugar:

    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            jugar = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                jugar = False
    
    pos_x += 1
    if pos_x > ANCHO:
        pos_x = 0

    # Dibujos
    ventana.fill(NEGRO)
    pygame.draw.rect(ventana, VERDE, (pos_x, pos_y, 50, 50))

    
    pygame.display.update()